package com.anywhere.doctor.repository;

import com.anywhere.doctor.domain.RoleMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RoleMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoleMasterRepository extends JpaRepository<RoleMaster, Long> {
}
