package com.anywhere.doctor.mapper;

import com.anywhere.doctor.dto.UserDTO;
import com.anywhere.doctor.Service.RoleMasterService;
import com.anywhere.doctor.domain.RoleMaster;
import com.anywhere.doctor.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserMapper {
    @Autowired
    private RoleMasterService roleMasterService;

    public User toEntity(UserDTO userDTO){
        User user=new User();

        if(userDTO.getId()!=null){
            user.setId(userDTO.getId());
        }
        user.setEmailId(userDTO.getEmailId());
        user.setContactNo(userDTO.getContactNo());

        Optional<RoleMaster> roleMaster= roleMasterService.findOne(Long.valueOf(userDTO.getRole()));
        if(roleMaster.isPresent())
            user.setRoleMaster(roleMaster.get());

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setActive(userDTO.isIsActive());

        user.setAddress(userDTO.getAddressList());

        return user;
    }

    public UserDTO toDto(User user){

        UserDTO userDTO=new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmailId(user.getEmailId());
        userDTO.setContactNo(user.getContactNo());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setIsActive(user.getActive());
        userDTO.setAddressList(user.getAddress());

        return userDTO;

    }
    
}
