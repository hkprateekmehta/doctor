package com.anywhere.doctor.mapper;


import com.anywhere.doctor.dto.RolemasterDTO;
import com.anywhere.doctor.domain.RoleMaster;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RolemasterMapper extends EntityMapper<RolemasterDTO, RoleMaster> {



    default RoleMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        RoleMaster rolemaster = new RoleMaster();
        rolemaster.setId(id);
        return rolemaster;
    }
}
