package com.anywhere.doctor.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A RoleMaster.
 */
@Entity
@Table(name = "rolemaster")
public class RoleMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String rolename;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RoleMaster{" +
            "id=" + getId() +
            ", role='" + getRolename() + "'" +
            "}";
    }
}
