package com.anywhere.doctor.resourse;

import com.anywhere.doctor.dto.UserDTO;
import com.anywhere.doctor.Service.UserService;
import com.anywhere.doctor.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    /**
     * {@code POST  /users} : Create a new user.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userDTO, or with status {@code 400 (Bad Request)} if the user has already an ID.
     * @param userDTO the userDTO to create.
     */
    @PostMapping("/users")
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) throws Exception {
        log.debug("REST request to save User : {}", userDTO);
        if (userDTO.getId() != null) {
            throw new Exception("A new user cannot already have an ID");
        }
        UserDTO result = userService.save(userDTO);
        return ResponseEntity.created(new URI("/api/users/" + result.getId()))
                .body(result);
    }


    /**
     * {@code PUT  /users} : Updates an existing user.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userDTO,
     * or with status {@code 400 (Bad Request)} if the userDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userDTO couldn't be updated.
     * @param userDTO the userDTO to update.
     */
    @PutMapping("/users")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO) throws Exception {
        log.debug("REST request to update User : {}", userDTO);
        if (userDTO.getId() == null) {
            throw new Exception("Invalid id null");
        }
        UserDTO result = userService.save(userDTO);
        return ResponseEntity.ok()
                .body(result);
    }

    /**
     * {@code GET  /users} : get all the users.
     *
     * @param pageable the pagination information.
     *      * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of users in body.
     */
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
        log.debug("REST request to get a page of Users");
        Page<UserDTO> pagedata = userService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pagedata);
        return ResponseEntity.ok().headers(headers).body(pagedata.getContent());
    }

    /**
     * {@code GET  /users/:id} : get the "id" user.
     *
     * @param id the id of the userDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/users/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
        log.debug("REST request to get User : {}", id);
        Optional<UserDTO> userDTO = userService.findOne(id);
        if (userDTO.isPresent())
            return ResponseEntity.ok().body(userDTO.get());

        return ResponseEntity.notFound().build();
    }


    /**
     * {@code GET  /users/inactive/{id}} : Soft Delete a user.
     *
     */
    @GetMapping("/users/inactive/{id}")
    public ResponseEntity<UserDTO> inactiveUser(@PathVariable Long id) throws Exception {
        log.debug("REST request to inactive User : {}", id);
        if (id == null) {
            throw new Exception("Cannot Inactive User as id doen'nt exist");
        }
        Optional<UserDTO> userDTO = userService.findOne(id);
        UserDTO result=new UserDTO();
        if(userDTO.isPresent()) {
            UserDTO dto=userDTO.get();
            dto.setIsActive(Boolean.FALSE);
            result = userService.save(dto);
            return ResponseEntity.ok().body(userDTO.get());
        }
        return ResponseEntity.badRequest().build();
    }


    /**
     * {@code DELETE  /users/:id} : delete the "id" user.
     *
     * @param id the id of the userDTO to delete.
     */
    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        log.debug("REST request to delete User : {}", id);

        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
