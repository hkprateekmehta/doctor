package com.anywhere.doctor.resourse;

import com.anywhere.doctor.Service.AddressService;
import com.anywhere.doctor.domain.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AddressResource {

    private final Logger log = LoggerFactory.getLogger(AddressResource.class);

    private String applicationName="Doctor Anywhere";

    private final AddressService addressService;

    public AddressResource(AddressService addressService) {
        this.addressService = addressService;
    }
/*

    */
/**
     * {@code POST  /addresses} : Create a new address.
     *
     * @param address the address to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new address, or with status {@code 400 (Bad Request)} if the address has already an ID.
     *//*

    @PostMapping("/addresses")
    public ResponseEntity<Address> createAddress(@RequestBody Address address) throws Exception {
        log.debug("REST request to save Address : {}", address);
        if (address.getId() != null) {
            throw new Exception("A new address cannot already have an ID");
        }
        Address result = addressService.save(address);
        return ResponseEntity.created(new URI("/api/addresses/" + result.getId()))
            .body(result);
    }

    */
/**
     * {@code PUT  /addresses} : Updates an existing address.
     *
     * @param address the address to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated address,
     * or with status {@code 400 (Bad Request)} if the address is not valid,
     * or with status {@code 500 (Internal Server Error)} if the address couldn't be updated.
     *//*

    @PutMapping("/addresses")
    public ResponseEntity<Address> updateAddress(@RequestBody Address address) throws Exception {
        log.debug("REST request to update Address : {}", address);
        if (address.getId() == null) {
            throw new Exception("Invalid id");
        }
        Address result = addressService.save(address);
        return ResponseEntity.ok()
            .body(result);
    }
*/

    /**
     * {@code GET  /addresses} : get all the addresses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of addresses in body.
     */
    @GetMapping("/addresses/{userId}")
    public List<Address> getAllAddresses(@PathVariable Long userId) {
        log.debug("REST request to get all Addresses");
        return addressService.findAll(userId);
    }


    /**
     * {@code DELETE  /addresses/:id} : delete the "id" address.
     *
     * @param id the id of the address to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/addresses/{id}")
    public ResponseEntity<Void> deleteAddress(@PathVariable Long id) {
        log.debug("REST request to delete Address : {}", id);

        addressService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
