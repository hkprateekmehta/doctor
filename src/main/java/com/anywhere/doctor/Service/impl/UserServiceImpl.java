package com.anywhere.doctor.Service.impl;

import com.anywhere.doctor.dto.UserDTO;
import com.anywhere.doctor.Service.UserService;
import com.anywhere.doctor.domain.Address;
import com.anywhere.doctor.domain.User;
import com.anywhere.doctor.mapper.UserMapper;
import com.anywhere.doctor.repository.AddressRepository;
import com.anywhere.doctor.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    @Value("${page.users.size}")
    private int pageSize;

    @Autowired
    private AddressRepository addressRepository;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    /**
     * Save a user.
     *
     * @param userDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserDTO save(UserDTO userDTO) {
        log.debug("Request to save User : {}", userDTO);
        User user = userMapper.toEntity(userDTO);
        user = userRepository.save(user);
        List<Address> addresses=userDTO.getAddressList();
        for(Address address:addresses){
            address.setUser(user);
            Address a=addressRepository.save(address);
        }
        return userMapper.toDto(user);
    }

    /**
     * Get all the users.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Users");

        pageable = PageRequest.of(pageable.getPageNumber(), pageSize);
        return userRepository.findAll(pageable)
            .map(userMapper::toDto);
    }


    /**
     * Get one user by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserDTO> findOne(Long id) {
        log.debug("Request to get User : {}", id);
        return userRepository.findById(id).map(userMapper::toDto);
    }

    /**
     * Delete the user by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete User : {}", id);

        userRepository.deleteById(id);
    }
}
