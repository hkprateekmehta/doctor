package com.anywhere.doctor.Service.impl;

import com.anywhere.doctor.Service.RoleMasterService;
import com.anywhere.doctor.domain.RoleMaster;
import com.anywhere.doctor.mapper.RolemasterMapper;
import com.anywhere.doctor.repository.RoleMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RoleMasterServiceImpl implements RoleMasterService {

    private final Logger log = LoggerFactory.getLogger(RoleMasterServiceImpl.class);

    private final RoleMasterRepository rolemasterRepository;


    public RoleMasterServiceImpl(RoleMasterRepository rolemasterRepository, RolemasterMapper rolemasterMapper) {
        this.rolemasterRepository = rolemasterRepository;
    }

    /**
     * Get one rolemaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RoleMaster> findOne(Long id) {
        log.debug("Request to get Rolemaster : {}", id);
        return rolemasterRepository.findById(id);
    }
}
