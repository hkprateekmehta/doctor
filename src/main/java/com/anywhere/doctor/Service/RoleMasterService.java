package com.anywhere.doctor.Service;


import com.anywhere.doctor.domain.RoleMaster;

import java.util.Optional;

public interface RoleMasterService {

    /**
     * Get the "id" rolemaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RoleMaster> findOne(Long id);

}
