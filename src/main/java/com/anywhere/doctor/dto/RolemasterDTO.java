package com.anywhere.doctor.dto;

import java.io.Serializable;

public class RolemasterDTO implements Serializable {
    
    private Long id;

    private String rolename;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RolemasterDTO)) {
            return false;
        }

        return id != null && id.equals(((RolemasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RolemasterDTO{" +
            "id=" + getId() +
            ", rolename='" + getRolename() + "'" +
            "}";
    }
}
