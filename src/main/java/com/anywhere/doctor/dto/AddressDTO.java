package com.anywhere.doctor.dto;

import java.io.Serializable;

public class AddressDTO implements Serializable {

    private Long id;

    private Long userId;

    private String line1;

    private String line2;

    private String city;

    private String country;

    private Integer postalcode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(Integer postalcode) {
        this.postalcode = postalcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AddressDTO)) {
            return false;
        }

        return id != null && id.equals(((AddressDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AddressDTO{" +
                "id=" + getId() +
                ", userId=" + getUserId() +
                ", line1='" + getLine1() + "'" +
                ", line2='" + getLine2() + "'" +
                ", city='" + getCity() + "'" +
                ", country='" + getCountry() + "'" +
                ", postalcode=" + getPostalcode() +
                "}";
    }
}
